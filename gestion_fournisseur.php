<h2>Gestion des fournisseurs</h2>

<?php
if (isset($_SESSION['email']) and $_SESSION['role'] == "admin") {
	$unControleur->setTable("fournisseur"); 
	$leFournisseur = null; 
	if (isset($_GET['action']) and isset($_GET['idfournisseur']))
	{
		$action = $_GET['action']; 
		$idfournisseur = $_GET['idfournisseur'];

		switch ($action)
		{
			case "sup" : 
				$where = array("idfournisseur"=>$idfournisseur); 
				$unControleur->delete ($where); 
				break;
			case "edit" : 
				$where = array("idfournisseur"=>$idfournisseur); 
				$leFournisseur = $unControleur->selectWhere($where); 
				break; 
		} 
	}

	require_once("vue/vue_insert_fournisseur.php"); 

	if(isset($_POST['Modifier']))
	{
		$unControleur->setTable("fournisseur");
		$tab = array("nom"=>$_POST['nom'], 
					 "email"=>$_POST['email'], 
					 "tel"=>$_POST['tel'],
					 "adresse"=>$_POST['adresse'] 
					);
		$where = array("idfournisseur"=>$_GET['idfournisseur']);
		$unControleur->update($tab, $where); 
		header("Location: index.php?page=2"); 
	}

	if(isset($_POST['Valider']))
	{
		$unControleur->setTable("fournisseur");
		$tab = array("nom"=>$_POST['nom'], 
					 "email"=>$_POST['email'], 
					 "tel"=>$_POST['tel'],
					 "adresse"=>$_POST['adresse'] 
					);
		$unControleur->insert($tab); 
	}
}

	$unControleur->setTable("fournisseur"); 
	if (isset($_POST['Rechercher']))
	{
		$tab = array("nom", "email", "tel", "adresse"); 
		$mot = $_POST['mot']; 
		$lesFournisseurs = $unControleur->selectSearch($tab, $mot); 
	}
	else
	{
		$lesFournisseurs = $unControleur->selectAll();
	}
	require_once ("vue/vue_les_fournisseurs.php"); 
?>