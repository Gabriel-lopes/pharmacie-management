<h2>Gestion des pharmacies</h2>

<?php
    if(isset($_SESSION['email']) and $_SESSION['role'] == "admin") {
    	$unControleur->setTable("pharmacie"); 
    	$laPharmacie = null; 

    	if (isset($_GET['action']) and isset($_GET['idpharmacie']))
    	{
    		$action = $_GET['action']; 
    		$idpharmacie = $_GET['idpharmacie'];

    		switch ($action)
    		{
    			case "sup" : 
    				$where = array("idpharmacie"=>$idpharmacie); 
    				$unControleur->delete ($where); 
    				break;
    			case "edit" : 
    				$where = array("idpharmacie"=>$idpharmacie); 
    				$laPharmacie = $unControleur->selectWhere($where); 

    				break; 
    		} 
    	}


    	require_once ("vue/vue_insert_pharmacie.php");
    	if(isset($_POST['Modifier']))
    	{
    		$unControleur->setTable("pharmacie");
    		$tab = array("nom"=>$_POST['nom'], 
    					 "tel"=>$_POST['tel'], 
    					 "email"=>$_POST['email'],
    					 "adresse"=>$_POST['adresse']
    					);
    		$where = array("idpharmacie"=>$_GET['idpharmacie']);
    		$unControleur->update($tab, $where); 
    		header("Location: index.php?page=1"); 
    	}

    	if(isset($_POST['Valider']))
    	{
    		$unControleur->setTable("pharmacie");
    		$tab = array("nom"=>$_POST['nom'], 
    					 "tel"=>$_POST['tel'], 
    					 "email"=>$_POST['email'],
    					 "adresse"=>$_POST['adresse']
    					);
    		$unControleur->insert($tab); 
    	}

    }

	$unControleur->setTable("pharmacie"); 
	if (isset($_POST['Rechercher']))
	{
		$tab = array("nom", "tel", "email", "adresse"); 
		$mot = $_POST['mot']; 
		$lesPharmacies = $unControleur->selectSearch($tab, $mot); 
	}
	else
	{
		$lesPharmacies = $unControleur->selectAll();
	}
	require_once ("vue/vue_les_pharmacies.php"); 
?>