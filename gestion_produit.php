<h2>Gestion des produits</h2>

<?php
if (isset($_SESSION['email']) and $_SESSION['role'] == "admin") {
	$unControleur->setTable("fournisseur"); 
	$lesFournisseurs = $unControleur->selectAll();

	$unControleur->setTable("produit"); 
	$leProduit = null; 
	if (isset($_GET['action']) and isset($_GET['idproduit']))
	{
		$action = $_GET['action']; 
		$idproduit = $_GET['idproduit'];

		switch ($action)
		{
			case "sup" : 
				$where = array("idproduit"=>$idproduit); 
				$unControleur->delete ($where); 
				break;
			case "edit" : 
				$where = array("idproduit"=>$idproduit); 
				$leProduit = $unControleur->selectWhere($where); 
				break; 
		} 
	}

	require_once ("vue/vue_insert_produit.php"); 

	if(isset($_POST['Modifier']))
	{
		$unControleur->setTable("produit");
		$tab = array("nom"=>$_POST['nom'], 
					 "typedeproduit"=>$_POST['typedeproduit'], 
					 "prix"=>$_POST['prix'],
					 "idfournisseur"=>$_POST['idfournisseur']
					);
		$where = array("idproduit"=>$_GET['idproduit']);
		$unControleur->update($tab, $where); 
		header("Location: index.php?page=3"); 
	}

	if(isset($_POST['Valider']))
	{
		$unControleur->setTable("produit");
		$tab = array("nom"=>$_POST['nom'], 
					 "typedeproduit"=>$_POST['typedeproduit'], 
					 "prix"=>$_POST['prix'],
					 "idfournisseur"=>$_POST['idfournisseur']
					);
		$unControleur->insert($tab);
	}
}

	$unControleur->setTable("produits_fournisseurs"); 
	if (isset($_POST['Rechercher']))
	{
		$tab = array("nom", "typedeproduit", "prix", "fournisseur"); 
		$mot = $_POST['mot']; 
		$lesProduits = $unControleur->selectSearch($tab, $mot); 
	}
	else
	{
		$lesProduits = $unControleur->selectAll();
	}
	require_once ("vue/vue_les_produits.php"); 
?>