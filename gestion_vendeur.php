<h2>Gestion des vendeurs</h2>

<?php
if (isset($_SESSION['email']) and $_SESSION['role'] == "admin") {
	$unControleur->setTable("pharmacie"); 
	$lesPharmacies = $unControleur->selectAll();

	$unControleur->setTable("vendeur"); 
	$leVendeur = null; 
	if (isset($_GET['action']) and isset($_GET['idvendeur']))
	{
		$action = $_GET['action']; 
		$idvendeur = $_GET['idvendeur'];

		switch ($action)
		{
			case "sup" : 
				$where = array("idvendeur"=>$idvendeur); 
				$unControleur->delete ($where); 
				break;
			case "edit" : 
				$where = array("idvendeur"=>$idvendeur); 
				$leVendeur = $unControleur->selectWhere($where); 
				break; 
		} 
	}

	require_once ("vue/vue_insert_vendeur.php"); 

	if(isset($_POST['Modifier']))
	{
		$unControleur->setTable("vendeur");
		$tab = array("nom"=>$_POST['nom'], 
					 "prenom"=>$_POST['prenom'], 
					 "email"=>$_POST['email'],
					 "adressePerso"=>$_POST['adressePerso'],
					 "idpharmacie"=>$_POST['idpharmacie'] 

					);
		$where = array("idvendeur"=>$_GET['idvendeur']);
		$unControleur->update($tab, $where); 
		header("Location: index.php?page=4"); 
	}

	if(isset($_POST['Valider']))
	{
		$unControleur->setTable("vendeur");
		$tab = array("nom"=>$_POST['nom'], 
					 "prenom"=>$_POST['prenom'], 
					 "email"=>$_POST['email'],
					 "adressePerso"=>$_POST['adressePerso'],
					 "idpharmacie"=>$_POST['idpharmacie'] 
					);
		$unControleur->insert($tab); 
	}

}
	$unControleur->setTable("vendeurs_pharmacies"); 
	if (isset($_POST['Rechercher']))
	{
		$tab = array("nom", "prenom", "email", "adressePerso", "pharmacie"); 
		$mot = $_POST['mot']; 
		$lesVendeurs = $unControleur->selectSearch($tab, $mot); 
	}
	else
	{
		$lesVendeurs = $unControleur->selectAll();
	}
	require_once ("vue/vue_les_vendeurs.php"); 
?>