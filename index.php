<?php
    session_start();
    require_once("controleur/config_bdd.php");
    require_once("controleur/controleur.class.php");
// instanciation de la classe Controleur
    $unControleur = new Controleur($server, $bdd, $user, $mdp);
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Site de renseignement des pharmacies </title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <center>
        <h1>Site de Gestion des pharmacies</h1>
        
        <?php
            if (!isset($_SESSION['email'])){
                require_once("vue/vue_connexion.php");
            }
            if(isset($_POST['SeConnecter']))
            {
                $where = array("email"=>$_POST['email'],
                               "mdp"=>$_POST['mdp']);
                $unControleur->setTable ("user");
                $unUser = $unControleur->selectWhere ($where);
                if (isset($unUser['email']))
                {
                    $_SESSION['email']= $unUser['email'];
                    $_SESSION['role']= $unUser['role'];
                    header("Location: index.php");
                }else{
                    echo "<br/> Veuillez verifier vos identifiants";
                }
            }
        if (isset($_SESSION['email'])){
            echo '
        
        
        <a href="index.php?page=0">
            <img src="images/home.png" height="100" width="100">
        </a>
        <a href="index.php?page=1">
            <img src="images/pharmacie.png" height="100" width="100">
        </a>
        <a href="index.php?page=2">
            <img src="images/fournisseur.png" height="100" width="100">
        </a>
        <a href="index.php?page=3">
            <img src="images/produit.png" height="100" width="100">
        </a>
        <a href="index.php?page=4">
            <img src="images/vendeur.png" height="100" width="100">
        </a>
        <a href="index.php?page=5">
            <img src="images/deconnexion.png" height="100" width="100">
        </a>
        ';
        
        if (isset($_GET['page'])) $page=$_GET['page'];
        else $page=0;

        switch($page){
            case 0:require_once("home.php"); break;
            case 1:require_once("gestion_pharmacie.php"); break;
            case 2:require_once("gestion_fournisseur.php"); break;
            case 3:require_once("gestion_produit.php"); break;
            case 4:require_once("gestion_vendeur.php"); break;
            case 5:unset($_SESSION); session_destroy();
            header("Location:index.php");
            break;
        }
        } //fin du if isset Session
        ?>
        
    </center>
</body>
</html>