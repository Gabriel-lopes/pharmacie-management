<?php
    class Modele
    {
        private $unPdo, $uneTable;

        public function __construct($server, $bdd, $user, $mdp)
        {
            $this->unPdo = null;
            
            try
            {
                $this->unPdo = new PDO("mysql:host=".$server.";dbname=".$bdd , $user, $mdp);
            }
            catch(PDOException $exp)
            {
                echo "Erreur de connecxion : ".$exp->getMessage();
            }
        }

        //l'attribut uneTable -> peremt de generer n'importe quelle table , function generique qui permet de generer n'importe qu'elle table, onl'utilise dans la function selectAll
        public function getTable()
        {
            return $this->uneTable;
        }

        public function setTable($uneTable)
        {
            $this->uneTable = $uneTable;
        }

        public function selectAll()
        {
            $requete = "select * from ".$this->uneTable.";";
            $select = $this->unPdo->prepare($requete);
            $select->execute();
            return $select->fetchAll();
        }
        public function insert($tab)
        {
            $champs = array();
            $donnes = array();
            foreach($tab as $cle => $valeur)
            {
                $champs[] = ":".$cle;
                $donnes[":".$cle] = $valeur;
            }
            $chaineChamps = implode(",", $champs);

            $requete = "insert into ".$this->uneTable." values (null,".$chaineChamps.");";
            //echo $requete ;
            $select = $this->unPdo->prepare($requete);
           // var_dump($donnes);
            $select->execute($donnes);
        }

        public function delete($where)
        {
            $champs = array();
            $donnes = array();

            foreach($where as $cle => $valuer)
            {
                $champs[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;

            }
            $chaineWhere = implode(" and ", $champs);
            $requete = "delete from ".$this->uneTable." where ".$chaineWhere; //une requete générique pour n'importe quelle table
            
            $delete = $this->unPdo->prepare($requete);
            $delete->execute($donnes);
        }
        public function selectWhere($where)//peremt de recuperer n'importe quelle donnée en spécifiant la clause 'where'
        {
            $champs = array();
            $donnes = array();

            foreach($where as $cle => $valuer)
            {
                $champs[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;

            }
            $chaineWhere = implode(" and ", $champs);
            $requete = "select * from ".$this->uneTable." where ".$chaineWhere; //une requete générique pour n'importe quelle table
            
            $select = $this->unPdo->prepare($requete);
            $select->execute($donnes);
            return $select->fetch(); // return un seul resultat avec fetch
        }
        public function update($tab, $where)
        {   //
            //c'est pour chaineWhere
            $champs = array();
            $donnes = array();

            foreach($where as $cle => $valuer)
            {
                $champs[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;

            }
            $chaineWhere = implode(" and ", $champs);
            


            // c'est pour chianechamps
            $champs2 = array();

            foreach($tab as $cle => $valuer)
            {
                $champs2[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;
            }
            $chaineChamps = implode(", ", $champs2);

            // apres on prépare la requete

            $requete = "update ".$this->uneTable." set ".$chaineChamps." where ".$chaineWhere; //une requete générique pour n'importe quelle table
            $update = $this->unPdo->prepare($requete);
            $update->execute($donnes);
        }

        public function selectSearch($tab, $mot)
        {
            $champs = array();
            $donnees = array(":mot"=>"%".$mot."%"); //le porucentage pour dire que ca commmence par .. et finit par ...
            foreach($tab as $cle)
            {
                $champs[] = $cle ." like :mot";
            }
            $chaineWhere = implode(" or ", $champs);

            $requete = "select * from ".$this->uneTable." where ".$chaineWhere;
            $select = $this->unPdo->prepare($requete);
            $select->execute($donnees);
            return $select->fetchAll();
        }
    }
?>