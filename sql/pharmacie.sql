drop database if exists pharmacieDataBase;
create database pharmacieDataBase;
use pharmacieDataBase;


create table pharmacie(
    idpharmacie int(3) not null auto_increment,
    nom varchar(50),
    tel varchar(50),
    email varchar(50),
    adresse varchar(50),
    primary key (idpharmacie)
);


create table vendeur(
    idvendeur int(3) not null auto_increment,
    nom varchar(50),
    prenom varchar(50),
    email varchar(50),
    adressePerso varchar(50),
    idpharmacie int(3),
    primary key (idvendeur),
    foreign key (idpharmacie) references pharmacie (idpharmacie)
);

create table fournisseur(
    idfournisseur int(3) not null auto_increment,
    nom varchar(50),
    email varchar(200),
    tel varchar(50),
    adresse varchar(50),
    primary key (idfournisseur)
);

create table produit(
    idproduit int(3) not null auto_increment,
    nom varchar(50),
    typedeproduit varchar(50),
    prix int(3),
    idfournisseur int(3),
    primary key (idproduit),
    foreign key (idfournisseur) references fournisseur (idfournisseur)
);

create table user(
    iduser int(3) not null auto_increment,
    nom varchar(50),
    prenom varchar(50),
    email varchar(50),
    mdp varchar(255),
    role enum("admin", "user"),
    primary key(iduser)
);

insert into pharmacie values (null,"Pharmacie de lyon", "123456789", "pharmacie@gmail.com", "2 RUE DE PARIS"),(null,"pharmacie de paris", "789456123","pharmacie2@gmail.com","30 rue de Lile");
insert into vendeur values (null,"Hamza", "Ismail", "h@gmail.com","rue de lyon", 1),(null,"Frederick", "Hendrickso", "fh@gmail.com", "rue de Lile", 2);
insert into fournisseur values (null, "Moderna","Moderna@gmail.com", "07525355", "rue de paris"), (null, "PFIZER", "PFIZER@gmail.com", "80082052", " rue de la paix");
insert into produit values (null, "SIROP", "medicament", 10),(null, "brosse Ã  dent", "produit", 5);
insert into user values(null, "Mustafa", "Gabriel", "ag@gmail.com", "123", "admin"), (null, "Naoki", "Thomas", "b@gmail.com","456", "user");

CREATE VIEW vendeurs_pharmacies AS SELECT v.idvendeur, v.nom, v.prenom, v.email, v.adressePerso, p.nom AS pharmacie FROM
vendeur v, pharmacie p WHERE v.idpharmacie = p.idpharmacie;

CREATE VIEW produits_fournisseurs AS SELECT p.idproduit, p.nom, p.typedeproduit, p.prix, f.nom AS fournisseur FROM
produit p, fournisseur f WHERE p.idfournisseur = f.idfournisseur;